function changeTab(elem, id) {
    $(".tab-btn").removeClass("active");
    $(elem).addClass("active");

    $("div[id*=tab]").css("display", "none");
    $("#tab"+id).css("display", "block");
}

//Выбираем тип
function changeType(elem) {
    var type = $(elem).val();
    if(type == "delete") {
        $("#tp_delete_group").css("display", "block");
        $("#tp_add_group").css("display", "none");
    }

    if(type == "add") {
        $("#tp_delete_group").css("display", "none");
        $("#tp_add_group").css("display", "block");
    }

}

//Выбираем причину исключения
function tpCauseSelect(elem){
    //Выберите
    if($(elem).val() == 0) {
        $("#cause, #delete_tp_list, #delete_tp_address, #delete_tp_contract, #delete_tp_date, #delete_tp_newcontract, #delete_tp_newuser").css("display", "none");
    }
    //Другая причина
    if($(elem).val() != 4) {
        $("#delete_tp_newcontract, #delete_tp_newuser").css("display", "none");
        $("#cause").css("display", "none");
        $("#delete_tp_list").css("display", "block");
    }else{
        $("#cause").css("display", "block");
        $("#delete_tp_list").css("display", "block");
    }

    if($(elem).val() == 3){
        $("#delete_tp_newcontract, #delete_tp_newuser").css("display", "block");
    }
}

//Выбираем точку исключения
function deleteTpSelect(elem) {
    var value = $(elem).val();
    console.log(value);
    if(value == 0) {
        $("#delete_tp_address, #delete_tp_contract").css("display", "none");
    }

    //Точка 1
    if(value == 1) {
        console.log("ТП 1");
        $("#delete_tp_address, #delete_tp_contract, #delete_tp_date").css("display", "block");
        $("#delete_tp_address input").val("г. Уфа. ул Коммунистическая 45/3");
        $("#delete_tp_contract input").val("ДПГ/01/2015");
    }
    //Точка 2
    if(value == 2) {
        console.log("ТП 2");
        $("#delete_tp_address, #delete_tp_contract, #delete_tp_date").css("display", "block");
        $("#delete_tp_address input").val("г. Уфа. ул Ленина 99");
        $("#delete_tp_contract input").val("ДПГ/02/2015");
    }
}

//Выбирием причину включения
function tpAddCauseSelect(elem) {
    //Выберите
    if($(elem).val() == 0) {
        $("#causeAdd").css("display", "none");
        $("#add_tp_name, #add_tp_address, #add_tp_contract_to, #add_tp_contract_from, #add_old_user, #add_tp_date, #add_documents").css("display", "none");
    }
    //Другая причина
    if($(elem).val() != 2) {
        $("#causeAdd").css("display", "none");
        $("#add_tp_name, #add_tp_address, #add_tp_contract_to, #add_tp_contract_from, #add_old_user, #add_tp_date, #add_documents").css("display", "block");
    }else{
        $("#causeAdd").css("display", "block");
    }
}

//Распечатываем
function printDelete(){
    var causeVal = $("#tp_delete_cause").val();
    var form = $("#main-form");
    if(causeVal == "3") {
        $(form).attr("action", "/delete_2.php").submit();
    }
    if(causeVal == "1" || causeVal == "2") {
        $(form).attr("action", "/delete_1.php").submit();
    }
}
//Распечатываем
function printAdd(){
    var causeVal = $("#tp_add_cause").val();
    var form = $("#main-form");
    if(causeVal > 0) {
        $(form).attr("action", "/add_1.php").submit();
    }

}

$('.datepicker').datepicker({
    format: "dd.mm.yyyy",
    language: "ru"
});

$(function () {
    $('[data-toggle="popover"]').popover()
})

//Добавление новой площадки
$('#add_new_form').on('click', function(){
    $('#current').hide(200);
    $('#new').removeClass('hide');
});

//Подчет итого в окне точки подключекния
$("#volume, #gaz, #pssu, #ttr").keyup(function(){
    checkout();
});

var volume = 0;
var gaz = 0;
var pssu = 0;
var ttr = 0;
var withoutNds = 0;
var withNds = 0;


function checkout() {
    
    if($('#volume').val() > 0)	{
        volume = $('#volume').val()
    }
    console.log("volume " + volume);
    
    if($('#gaz').val() > 0)	{
        gaz = $('#gaz').val()
    }
    console.log("gaz " + gaz);
    if($('#pssu').val() > 0)	{
        pssu = $('#pssu').val()
    }
    console.log("pssu " + pssu);
    if($('#ttr').val() > 0)	{
        ttr = $('#ttr').val()
    }
    console.log("ttr " + ttr);

    withoutNds = parseInt(volume) * (parseInt(gaz) + parseInt(pssu) + parseInt(ttr));
    withNds = withoutNds * 1.18;

    $('#withoutNds').val(withoutNds);
    $('#withNds').val(withNds);
}



