<? require_once "view/header.php" ?>
<? require_once "left_menu.php" ?>

<div class="col-md-9" style="margin-top: 15px">
	<h1 class="header header1">Главная страница</h1>
</div>


<? require_once "view/footer.php" ?>


<div class="modal fade files" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header blue"><h1 class="header">Смена пароля</h1></div>
			<div class="modal-body">
				<form action="#">
					<div class="form-group">
						<label for="exampleInputName">Старый пароль</label>
						<input type="password" class="form-control" id="old_password" name="old_password"
						       placeholder="старый пароль">
					</div>
					<div class="form-group">
						<label for="exampleInputName">Новый пароль</label>
						<input type="password" class="form-control" id="password" name="password"
						       placeholder="новый пароль">
					</div>
					<div class="form-group">
						<label for="exampleInputName">Повторите пароль</label>
						<input type="password" class="form-control" id="re-password" name="re-password" placeholder="">
					</div>

				</form>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn blue">ОК</button>
				<button data-dismiss="modal" class="btn btn-danger">Отмена</button>
			</div>
		</div>
	</div>
</div>

<script>
	$('.files').modal('show')
</script>

</body>
</html>