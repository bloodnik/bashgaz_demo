$(window).load(function() {
    $('#send-error').on('click',function(){
        //собственно включение самого html2canvas
        html2canvas(document.body, {
            onrendered: function(canvas) {
                var data = canvas.toDataURL('image/png').replace(/data:image\/png;base64,/, '');
                $.post('/lib/savePic.php',{data:data}, function(rep){
                    $('#screenshot').attr('src', '/lib/'+rep);
                });
            }
        });
    });

    //Добавление в таблицу строки
    var prop_row = $('.prop_row');
    $("#add_prop").on('click', function() {
        $('#props_body .prop_row:last').after(prop_row[0].outerHTML);
    })

    //Тултипы
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    //Вывод алерта - демо режим
    $(".demo").on("click", function(e){
        $("#alert-msg").slideDown(200);
        setTimeout(function(){
            $("#alert-msg").slideUp(200);
        }, 1500);
    });

    //Древовидная таблица. Документация: http://maxazan.github.io/jquery-treegrid/examples/example-bootstrap-3.html
    $('.tree').treegrid({
        expanderExpandedClass: 'glyphicon glyphicon-minus',
        expanderCollapsedClass: 'glyphicon glyphicon-plus'
    });

    //Выбор даты
    $('.datepicker').datepicker({
        format: "dd.mm.yyyy",
        language: "ru",
        autoclose: true,
    });


    $(function () {
        $('[data-toggle="popover"]').popover()
    });

});

//Подчет итого в окне точки подключекния
$("#volume, #gaz, #pssu, #ttr").keyup(function(){
    checkout();
});

var volume = 0;
var gaz = 0;
var pssu = 0;
var ttr = 0;
var withoutNds = 0;
var withNds = 0;


function checkout() {
    
    if($('#volume').val() > 0)	{
        volume = $('#volume').val()
    }
    console.log("volume " + volume);
    
    if($('#gaz').val() > 0)	{
        gaz = $('#gaz').val()
    }
    console.log("gaz " + gaz);
    if($('#pssu').val() > 0)	{
        pssu = $('#pssu').val()
    }
    console.log("pssu " + pssu);
    if($('#ttr').val() > 0)	{
        ttr = $('#ttr').val()
    }
    console.log("ttr " + ttr);

    withoutNds = parseInt(volume) * (parseInt(gaz) + parseInt(pssu) + parseInt(ttr));
    withNds = withoutNds * 1.18;

    $('#withoutNds').val(withoutNds);
    $('#withNds').val(withNds);
}



