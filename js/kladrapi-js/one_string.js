$(function () {
    var $address = $('[name="address"]');
    var city = $('[name="city"]');
    var street = $('[name="street"]');

    $address.kladr({
        oneString: true,
        change: function (obj) {
            log(obj);
        }
    });

    $('[name="country"]').kladr({
        type: $.kladr.type.region
    });

    city.kladr({
        type: $.kladr.type.city,
        select: function (obj) {
            street.kladr('parentType', $.kladr.type.city);
            street.kladr('parentId', obj.id);
        },
    });

    street.kladr({
        type: $.kladr.type.street,
    });

    function log(obj) {
        var $log, i;

        $('.js-log li').hide();


        for (i in obj) {
            $log = $('#' + i);

            if ($log.length) {
                $log.find('.value').text(obj[i]);
                $log.show();
            }
        }
    }
});
