<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9 pay-methods" style="margin-top: 15px">
		<div class="row">
			<h1 class="header">Способы оплаты</h1>
		</div>
		<div class="row">
			<div class="col-md-6">
				<h2 class="header header2">Оплата пластиковой картой</h2>
				<div class="thumbnail">
					<img src="/img/visa_mastercard.jpg" alt="Оплата пластиковой картой">
				</div>
			</div>
			<div class="col-md-6">
				<h2 class="header header2">Оплата с расчетного счета</h2>
				<div class="thumbnail">
					<img src="/img/bill.jpg" alt="Оплата с расчетного счета">
				</div>
			</div>
		</div>
	</div>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>