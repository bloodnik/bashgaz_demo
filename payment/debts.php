<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<div class="row">
			<h1 class="header">Информация по задолженности</h1>

			<form action="#" class="form-inline">
				<label>Период</label>
				<div class="input-group">
					<input type="text" class="form-control input-sm datepicker" name="from">
					<span class="input-group-addon" id="sizing-addon2">-</span>
					<input type="text" class="form-control input-sm datepicker" name="to">
				</div>

				<div class="form-group">
					<label>№ договора</label>
					<select name="current_contract" id="" class="form-control input-sm">
						<option value="№1354 от 20.04.2016">№1354 от 20.04.2016</option>
						<option value="№464 от 06.04.2016">№464 от 06.04.2016</option>
					</select>
				</div>

				<button class="btn light-blue btn-sm demo"><i class="glyphicon glyphicon-transfer"></i> Сформировать</button>
			</form>
			<div class="clerafix"></div>
			<hr>

			<table class="table table-bordered table-hover">
				<thead>
				<tr>
					<th>Месяц</th>
					<th>Начальный остаток</th>
					<th>Отгружено, руб</th>
					<th>Отгружено, куб.м.</th>
					<th>Оплачено, руб.</th>
					<th>Конечный остаток</th>
				</tr>
				<tr style="background: #eff2ff">
					<th>Итого</th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
				</tr>
				</thead>

				<tr>
					<td>Январь</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Февраль</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Март</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Апрель</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>...</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>

			<div class="pull-right">
				<button class="btn light-blue demo">Сформировать акт сверки</button>
				<button class="btn light-blue dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					Сформировать счет
					<span class="caret"></span>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
					<li><a href="#" class="demo">Счет на задолженность</a></li>
					<li><a href="#" class="demo">Счет на объем поставки</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<br><br><br><br><br>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>