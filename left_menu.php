<div class="col-md-3" style="margin-top: 15px">
	<ul class="multi-list">
		<li>
			<span class="header header2"><a href="/main/contracts/">Договоры</a></span>
			<ul>
				<li><span class="sub-item"><a href="/main/contracts/">Доп. соглашения</a></span></li>
				<li><span class="sub-item"><a href="/main/contracts/">Акты реализации</a></span></li>
				<li><span class="sub-item"><a href="/main/contracts/">Акты сверки</a></span></li>
				<li><span class="sub-item"><a href="/main/contracts/">Судебные иски</a></span></li>
			</ul>
		</li>
		<li>
			<span class="header header2">Данные по заявкам</span>
			<ul>
				<li class="sub-item"><a href="/main/services-list/">Список заявок</a></li>
			</ul>
		</li>

		<li>
			<span class="header header2">Создать заявку</span>
			<ul>
				<li class="sub-item"><a href="/main/services/cancel_contract.php">Заявки на расторжение договора</a></li>
				<li class="sub-item"><a href="/main/services/change_volume.php">Заявки на изменение объемов договора поставки газов</a></li>
				<li class="sub-item"><a href="/main/services/delete_point.php">Заявки на исключение точки отбора газа из действующего договора</a></li>
				<li class="sub-item"><a href="/main/services/add_point.php">Заявки на включение точки отбора газа в действующий договор</a></li>
				<li class="sub-item"><a href="/main/services/change_props.php">Заявка на изменение реквизитов</a></li>
			</ul>
		</li>
	</ul>

</div>