<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Главная страница</title>

	<!--Стили-->
	<link rel="stylesheet" href="/css/animate.css"/>
	<link rel="stylesheet" href="/js/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/js/jasny-bootstrap/css/jasny-bootstrap.min.css"/>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/css/bootstrap-datepicker.min.css"/>
	<link rel="stylesheet" href="/js/bootstrap-dropdown-hover/css/bootstrap-dropdownhover.min.css"/>
	<link href="/js/kladrapi-js/jquery.kladr.min.css" rel="stylesheet">
	<link href="/js/bootstrap-editable/css/bootstrap-editable.css" rel="stylesheet">
	<link href="/js/bootstrap-treegrid/css/jquery.treegrid.css" rel="stylesheet">

	<link rel="stylesheet" href="/css/colors.css"/>
	<link rel="stylesheet" href="/css/style.css"/>

	<script type="text/javascript" src="/js/jquery.js"></script>

</head>
<body>
<div class="container">
	<div class="row">
		<nav class="navbar navbar-fixed-top navbar-blue">
			<div class="container">
				<div class="collapse navbar-collapse main-menu" id="bs-example-navbar-collapse-animations" data-hover="dropdown" data-animations="fadeIn fadeIn fadeIn fadeIn">
					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a href="/main/" class="dropdown-toggle btn blue" data-toggle="dropdown" role="button" aria-expanded="false">Документы <span class="caret"></span></a>
							<ul class="dropdown-menu dropdownhover-bottom" role="menu" style="">
								<li><a href="/main/contracts/">Договоры</a></li>
								<li><a href="/main/services-list/">Данные по заявкам</a></li>
								<li><a href="/main/services/">Создание заявки</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="/mail/" class="dropdown-toggle btn blue" data-toggle="dropdown" role="button" aria-expanded="false">Письма <span class="caret"></span></a>
							<ul class="dropdown-menu dropdownhover-bottom" role="menu" style="">
								<li><a href="/mail/new.php">Написать письмо <i class="glyphicon glyphicon-pencil pull-right text-blue "></i></a></li>
								<li><a href="/mail/inbox.php">Входящие </a></li>
								<li><a href="/mail/outbox.php">Исходящие</a></li>
								<li><a href="/mail/deleted.php">Удаленные</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="/payment/" class="dropdown-toggle btn blue" data-toggle="dropdown" role="button" aria-expanded="false">Расчеты <span
									class="caret"></span></a>
							<ul class="dropdown-menu dropdownhover-bottom" role="menu" style="">
								<li><a href="/payment/debts.php">Информация по задолженностям</a></li>
								<li><a href="/payment/pay-methods.php">Способ оплаты</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="/additional/" class="dropdown-toggle btn blue" data-toggle="dropdown" role="button" aria-expanded="false">Профиль организации<span
									class="caret"></span></a>
							<ul class="dropdown-menu dropdownhover-bottom" role="menu" style="">
								<li><a href="/personal/org.php">Организация</a></li>
								<li><a href="/personal/bank.php">Банковские реквизиты</a></li>
								<li><a href="/personal/address.php">Адрес</a></li>
								<li><a href="/personal/director.php">Директор</a></li>
								<li><a href="/personal/contacts.php">Контактные данные</a></li>
								<li><a href="/personal/meters-data.php">Передать показания счетчиков</a></li>
							</ul>
						</li>
						<li class="dropdown">
							<a href="/faq/" class="dropdown-toggle btn blue" data-toggle="dropdown" role="button" aria-expanded="false">Вопросы <span
									class="caret"></span></a>
							<ul class="dropdown-menu dropdownhover-bottom" role="menu" style="">
								<li><a href="/faq/">Список часто задаваемых вопросов</a></li>
							</ul>
						</li>
					</ul>
					<ul class="nav navbar-nav pull-right">
						<li><a href="#" class="btn light-blue" id="send-error" data-toggle="modal" data-target=".send-error">Сообщить об ошибке</a></li>
					</ul>
				</div>
			</div>
			<div class="alert light-blue text-center" id="alert-msg" role="alert" style="display: none;position: absolute; top: 52px; opacity: .9; width: 100%; margin: 0; z-index: 9999;">
				<strong>Функционал не доступен в демо версии!</strong>
			</div>
		</nav>
	</div>
</div>

<div class="container main-page">
	<div class="col-md-12 main-header">
		<div class="row">
			<div class="col-md-2">
				<a href="/main.php">
					<img alt="" src="/img/logo.png" width="160px">

				</a>
			</div>
			<div class="col-md-3">
				<h1>Интерактивный интернет-сервис</h1>
			</div>
			<div class="col-md-4 pull-right">
				<ul class="info-list">
					<li><span>Организация:</span> ОАО Пример</li>
					<li><span>ИНН:</span> 000000000000</li>
					<li><span>Последний вход:</span> 23.03.2015 12:26:06</li>
					<li>
						<a href="/personal/org.php"><span class="glyphicon glyphicon-user"></span> Профиль</a>
						<a href="#"><span class="glyphicon glyphicon-time"></span> История</a>
						<a href="/index.php"><span class="glyphicon glyphicon-log-out"></span> Выход</a>
					</li>

				</ul>
			</div>
		</div>
	</div>


	<div class="col-md-12">
		<div class="row">

		</div>
	</div>