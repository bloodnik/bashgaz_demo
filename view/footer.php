</div>


<div class="modal fade send-error" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog ">

		<div class="modal-content">
			<div class="modal-header blue"><h1 class="header">Нашли ошибку?</h1></div>
			<div class="modal-body">
				<form action="#" class="">
					<div class="form-group">
						<label for="exampleInputEmail1">Введите описание произошедшей ошибки</label>
						<textarea class="form-control" rows="3"></textarea>
					</div>

					<img src="" alt="" id="screenshot" style="width: 100%;">

				</form>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn blue demo">Отправить</button>
				<button data-dismiss="modal" class="btn btn-danger">Отмена</button>
			</div>
		</div>

	</div>
</div>

<!--Скрипты-->
<script src="/js/kladrapi-js/jquery.kladr.min.js" type="text/javascript"></script>
<script src="/js/kladrapi-js/one_string.js" type="text/javascript"></script>

<script type="text/javascript" src="/js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/jasny-bootstrap/js/jasny-bootstrap.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.ru.min.js"></script>
<script type="text/javascript" src="/js/bootstrap-dropdown-hover/js/bootstrap-dropdownhover.min.js"></script>
<script type="text/javascript" src="/js/html2canvas.js"></script>
<script src="/js/bootstrap-editable/js/bootstrap-editable.min.js"></script>
<script src="/js/bootstrap-treegrid/js/jquery.treegrid.min.js"></script>
<script src="/js/bootstrap-treegrid/js/jquery.treegrid.bootstrap3.js"></script>

<script type="text/javascript" src="/js/main.js"></script>



</body>
</html>