<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Вход в личный кабинет</title>

	<!--Стили-->
	<link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css"/>

	<link rel="stylesheet" href="css/colors.css"/>
	<link rel="stylesheet" href="css/style.css"/>

	<!--Скрипты-->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<div class="row">
		<nav class="navbar navbar-fixed-top blue">
		</nav>
	</div>
</div>

<div class="container auth-form">
	<div class="row">
		<div class="col-md-12">
			<h1 class="text-center">
				Вход в интернет сервис
			</h1>
		</div>
		<div class="col-md-4 col-md-offset-4">
			<form action="/main.php" name="form" class="form-horizontal">

				<div class="col-md-12">
					<div class="form-group">
						<input type="text" value="" placeholder="Введите ИНН" id="login"
					                                class="form-control"/>
					</div>
				</div>
				<div class="col-md-12">
					<div class="form-group">
						<input type="text" value="" placeholder="Введите пароль" id="pass" class="form-control"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-12">
						<input type="submit" class="btn blue pull-left" value="Войти"/>
						<a href="#" class="btn light-blue pull-right">Войти используя ЭЦП</a>
					</div>
				</div>


			</form>

			<div class="col-md-4">
				<div class="row"><a href="/forget_pass.html">Забыли пароль?</a></div>
			</div>
			<div class="col-md-8">
				<div class="row pull-right"><a href="/anketa.html">Хотите получить доступ к сервису?</a></div>
			</div>

		</div>
	</div>
</div>

</body>
</html>