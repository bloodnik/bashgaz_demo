<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header">Заявка на изменение объемов договора поставки газа</h1>

		<form class="is-form" method="get" action="../print_forms/change_volume_up.php">
			<div class="form-group">
				<label>№ договора/контракта</label>
				<select name="current_contract" id="" class="form-control">
					<option value="№1354 от 20.04.2016">№1354 от 20.04.2016</option>
					<option value="№464 от 06.04.2016">№464 от 06.04.2016</option>
				</select>
			</div>
			<div class="form-group">
				<label>Наименование точки подключения</label>
				<select name="tp_name" id="" class="form-control">
					<option value="Котельная №16 п.Зирган">Котельная №16 п.Зирган</option>
					<option value="Котельная №19 с.Нордовка">Котельная №19 с.Нордовка</option>
				</select>
			</div>
			<div class="form-group">
				<label>Наименование площадки</label>
				<select name="tp_ploshadka" id="" class="form-control">
					<option value="Котельная №16 п.Зирган">Котельная №16 п.Зирган</option>
					<option value="Котельная №19 с.Нордовка">Котельная №19 с.Нордовка</option>
				</select>
			</div>
			<div class="form-group">
				<label>Адрес площадки</label>
				<input type="text" class="form-control" id="exampleInputAddress" name="address" placeholder="Адрес">
			</div>
			<div class="form-group">
				<label>Тип заявки</label>
				<select name="type" id="type" class="form-control">
					<option value="На увеличение объемов">На увеличение объемов</option>
					<option value="На снижение объемов">На снижение объемов</option>
				</select>
			</div>

			<div class="panel panel-default">
				<div class="panel-body">

					<div class="form-inline">
						<div class="form-group">
							<label for="exampleInputName2">От </label>
							<select class="form-control">
								<option>Январь</option>
								<option selected>Февраль</option>
								<option>Март</option>
								<option>Апрель</option>
								<option>Май</option>
								<option>...</option>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail2"> До </label>
							<select class="form-control">
								<option>Январь</option>
								<option>Февраль</option>
								<option>Март</option>
								<option selected>Апрель</option>
								<option>Май</option>
								<option>...</option>
							</select>
						</div>
						<button type="button" class="btn light-blue  demo">Заполнить</button>
					</div>
					<br>
					<br>
					<table class="table table-bordered">
						<thead>
						<tr>
							<th>Период</th>
							<th>Существующий объем, тыс.куб.м.</th>
							<th>Изменение, тыс.куб.м.</th>
							<th>Итого, тыс.куб.м.</th>
						</tr>
						</thead>
						<tr>
							<td>Февраль</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Март</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Апрель</td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
					</table>
					<a href="javascript:void(0)" class="btn btn-info pull-right  demo">Выгрузить заявку на поставку газа</a>

					<br><br>

					<h3 class="header header3">Прикрепленные файлы*</h3>

					<ul class="list-unstyled">
						<li>
							<div class="col-md-6">
								<span class="file-name">Заявка на поставку газа с приложениями в соотвествии с изменениями объемов</span>
							</div>
							<div class="col-md-6">
								<input type="file" id="exampleInputFile" class="file-input pull-left"/>
								<span class="small text-success">файл прикреплен</span>
							</div>
							<div class="clearfix"></div>
						</li>

					</ul>

					<small class="text-info">*Все документы должны быть в формате PDF и читабельными</small>

					<br>
					<br>
					<div class="btn-group btn-group-justified" role="group">
						<a class="btn btn-default light-blue" role="button" id="generate_letter">Сформировать письмо <span class="glyphicon glyphicon-list-alt" area-hidden="true"></span></a>
						<a class="btn btn-success demo">Подписать <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></a>
						<a class="btn btn-default demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
						<a class="btn btn-default demo">Отправить <span class="glyphicon glyphicon-send" aria-hidden="true"></span></a>
					</div>

				</div>
		</form>

	</div>


	<script>
		$('#generate_letter').on('click', function () {
			var form = $(".is-form");
			$(form).submit();

		});

		$("#type").on("change", function () {
			if ($(this).val() == 'На увеличение объемов') {
				$('.is-form').attr("action", "../print_forms/change_volume_up.php");
			} else {
				$('.is-form').attr("action", "../print_forms/change_volume_down.php");
			}
		})


	</script>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>