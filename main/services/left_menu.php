<div class="col-md-3" style="margin-top: 15px">
	<ul class="multi-list">
		<li>
			<span class="header header2"><a href="cancel_contract.php">Заявка на расторжение ДПГ</a></span>
		</li>
		<li>
			<span class="header header2">Заявки на изменение объемов ДПГ</span>
			<ul>
				<li class="sub-item"><a href="change_volume.php">Заявка на изменение объемов ДПГ</a></li>
				<li class="sub-item"><a href="change_volume_fin.php">Заявка на изменение объемов ДПГ (по выделенному финансированию)</a></li>
			</ul>
		</li>
		<li>
			<span class="header header2"><a href="delete_point.php">Заявка на исключение точки отбора газа из действующего ДПГ</a></span>
		</li>
		<li>
			<span class="header header2"><a href="add_point.php">Заявка на включение точки отбора газа в действующий ДПГ</a></span>
		</li>
		<li>
			<span class="header header2"><a href="change_props.php">Заявка на изменение реквизитов</a></span>
		</li>
	</ul>
</div>