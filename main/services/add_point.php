<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header"> Заявки на включение точки отбора газа в действующий ДПГ</h1>

		<form class="is-form" method="get" action="../print_forms/add_point.php">
			<div class="form-group">
				<label for="exampleInputName">№ договора/контракта</label>
				<select name="current_contract" id="" class="form-control">
					<option value="№1354 от 20.04.2016">№1354 от 20.04.2016</option>
					<option value="№464 от 06.04.2016">№464 от 06.04.2016</option>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleInputName">Причина вкллючения</label>
				<select name="cause" id="cause" class="form-control">
					<option value="постройкой">построена</option>
					<option value="передачей другой организации">передача другой организации</option>
				</select>
			</div>
			<div class="panel panel-info hide" id="cause-panel">
				<div class="panel-body">
					<div class="form-group">
						<label for="exampleInputName">Наименование организации (откуда поступила точка подлючения)</label>
						<input type="text" class="form-control" id="exampleInputName" name="org" placeholder="Введите наименование организации (ИНН/КПП)">
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="exampleInputName">Наименование точки подключения</label>
				<select name="tp_name" id="tp_name" class="form-control">
					<option value="Котельная №16 п.Зирган">Котельная №16 п.Зирган</option>
					<option value="Котельная №19 с.Нордовка">Котельная №19 с.Нордовка</option>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleInputName">Наименование площадки</label>
				<select name="tp_ploshadka" id="" class="form-control">
					<option value="Котельная №16 п.Зирган">Котельная №16 п.Зирган</option>
					<option value="Котельная №19 с.Нордовка">Котельная №19 с.Нордовка</option>
				</select>
			</div>
			<div class="form-group">
				<label>Адрес площадки</label>
				<input type="text" class="form-control" id="exampleInputAddress" name="address" placeholder="Адрес">
			</div>

			<a href="javascript:void(0)" class="btn btn-info pull-right  demo">Выгрузить заявку на поставку газа</a>
			<br><br>
			<div class="form-group">
				<label>Сумма выделенного бюджета, тыс. руб</label>
				<input type="text" class="form-control" id="exampleInputAddress" name="budjet" placeholder="Бюджет">
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Резервное топливо</label>
						<select name="rezerv" id="rezerv" class="form-control">
							<option value="предусмотрено">предусмотрено</option>
							<option value="не предусмотрено">не предусмотрено</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<label>&nbsp;</label>
					<input type="text" class="form-control" id="budjet" name="budjet" placeholder="Бюджет">
				</div>
			</div>

			<h3 class="header header3">Прикрепленные файлы*</h3>

			<ul class="list-unstyled files">
				<li>
					<div class="col-md-6">
						<span class="file-name">Заявка на поставку газа с приложениями</span>
					</div>
					<div class="col-md-6">
						<input type="file" id="exampleInputFile" class="file-input pull-left"/>
						<span class="small text-success">файл прикреплен</span>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="col-md-6">
						<span class="file-name">Копии документов, подтверждающих право собственности на газопотребляющий объект</span>
					</div>
					<div class="col-md-6">
						<input type="file" id="exampleInputFile" class="file-input pull-left"/>
						<span class="small text-success">файл прикреплен</span>
					</div>
					<div class="clearfix"></div>
				</li>
				<li>
					<div class="col-md-6">
						<span class="file-name">Копия акта приема - передачи оборудования или иной документ по передаче объекта</span>
					</div>
					<div class="col-md-6">
						<input type="file" id="exampleInputFile" class="file-input pull-left"/>
						<span class="small text-danger">не прикреплен</span>
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>

			<small class="text-info">*Все документы должны быть в формате PDF и читабельными</small>

			<br>
			<br>
			<div class="btn-group btn-group-justified" role="group">
				<a class="btn btn-default light-blue" role="button" id="generate_letter">Сформировать письмо <span class="glyphicon glyphicon-list-alt" area-hidden="true"></span></a>
				<a class="btn btn-success demo">Подписать <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></a>
				<a class="btn btn-default demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
				<a class="btn btn-default demo">Отправить <span class="glyphicon glyphicon-send" aria-hidden="true"></span></a>
			</div>
		</form>

	</div>


	<script>
		$('#generate_letter').on('click', function () {
			var form = $(".is-form");
			$(form).submit();

		});

		$("#rezerv").on("change", function () {
			if ($(this).val() == 'предусмотрено') {
				$('#budjet').removeClass("hide");
			} else {
				$('#budjet').addClass("hide");
			}
		});

	</script>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>