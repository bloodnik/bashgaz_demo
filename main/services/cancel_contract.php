<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header">Заявка на расторжение ДПГ</h1>

		<form class="is-form" method="get" action="../print_forms/cancel_contarct_likv.php">
			<div class="form-group">
				<label for="exampleInputName">№ договора/контракта</label>
				<select name="current_contract" id="" class="form-control">
					<option value="№1354 от 20.04.2016">№1354 от 20.04.2016</option>
					<option value="№464 от 06.04.2016">№464 от 06.04.2016</option>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleInputName">Наименование точки подключения</label>
				<select name="tp_name" id="" class="form-control">
					<option value="Котельная №16 п.Зирган">Котельная №16 п.Зирган</option>
					<option value="Котельная №19 с.Нордовка">Котельная №19 с.Нордовка</option>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleInputName">Наименование площадки</label>
				<select name="tp_ploshadka" id="" class="form-control">
					<option value="Котельная №16 п.Зирган">Котельная №16 п.Зирган</option>
					<option value="Котельная №19 с.Нордовка">Котельная №19 с.Нордовка</option>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleInputAddress">Адрес площадки</label>
				<input type="text" class="form-control" id="exampleInputAddress" name="address" placeholder="Адрес">
			</div>
			<div class="form-group">
				<label for="exampleInputName">Причина расторжения</label>
				<select name="cause" id="cause" class="form-control">
					<option value="ликвидация">ликвидация</option>
					<option value="передача другой организации">передача другой организации</option>
				</select>
			</div>

			<div class="panel panel-default hide" id="cause-panel">
				<div class="panel-body">
					<div class="panel-heading header header3">Новая организация</div>
					<div class="form-group">
						<label for="exampleInputName">Наименование огранизации (ИНН/КПП)</label>
						<input type="email" class="form-control" id="exampleInputName" name="org" placeholder="Наименование">
					</div>
					<div class="form-group">
						<label for="exampleInputAddress">№ Договор (нов.) (если имеется)</label>
						<input type="text" class="form-control" name="new_contract" id="new_contract" placeholder="Договор">
					</div>
				</div>
			</div>

			<h3 class="header header3">Прикрепленные файлы*</h3>

			<ul class="list-unstyled">
				<li>
					<div class="col-md-6">
						<span class="file-name">Копия акта об установлении заглушки, выданного ОАО "Газпром газораспределие Уфа"</span>
					</div>
					<div class="col-md-6">
						<input type="file" id="exampleInputFile" class="file-input pull-left"/>
						<span class="small text-success">файл прикреплен</span>
					</div>
					<div class="clearfix"></div>
				</li>

				<li>
					<div class="col-md-6">
						<span class="file-name">Копия акта опломбирования  выданного ООО "Газпром межрегионгаз Уфа"</span>
					</div>
					<div class="col-md-6">
						<input type="file" id="exampleInputFile" class="file-input pull-left"/>
						<span class="small text-danger">прикрепите файл</span>
					</div>
					<div class="clearfix"></div>
				</li>
			</ul>

			<small class="text-info">*Все документы должны быть в формате PDF и читабельными</small>

			<br>
			<br>
			<div class="btn-group btn-group-justified" role="group">
				<a class="btn btn-default light-blue" role="button" id="generate_letter">Сформировать письмо <span class="glyphicon glyphicon-list-alt" area-hidden="true"></span></a>
				<a class="btn btn-success demo">Подписать <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></a>
				<a class="btn btn-default demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
				<a class="btn btn-default demo">Отправить <span class="glyphicon glyphicon-send" aria-hidden="true"></span></a>
			</div>
		</form>
	</div>

	<br><br><br>

	<script>
		$('#generate_letter').on('click', function () {
			var form = $(".is-form");
			$(form).submit();

		});

		$("#cause").on("change", function () {
			if ($(this).val() == 'передача другой организации') {
				$('#cause-panel').removeClass("hide");
				$('.is-form').attr("action", "../print_forms/cancel_contarct_org.php");

			} else {
				$('#cause-panel').addClass("hide");
				$('.is-form').attr("action", "../print_forms/cancel_contarct_likv.php");
			}
		})


	</script>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>