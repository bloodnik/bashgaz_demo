<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header">Заявка на изменение реквизитов</h1>

		<form class="is-form" method="get" action="../print_forms/change_props.php">
			<div class="form-group">
				<label>Выбор объекта изменения</label>
				<select name="object" id="object" class="form-control">
					<option value="Организация">Организация</option>
					<option value="Договор">Договор</option>
				</select>
			</div>
			<div class="form-group">
				<label>Выбор раздела</label>
				<select name="rubric" id="rubric" class="form-control">
					<option value="Организация">Организация</option>
					<option value="Банковские реквизиты">Банковские реквизиты</option>
					<option value="Адрес">Адрес</option>
					<option value="Контакты">Контакты</option>
				</select>
			</div>

			<div class="form-group pull-right">
				<label>Прикрепить документ(один на все изменения)</label>
				<input type="file" class="btn light-blue" width="100%" name="general_doc" value="">
			</div>

			<div class="clearfix"></div>

			<hr>

			<div class="form-group">
				<a href="javascript:void(0)" class="btn light-blue demo">Заполнить</a>
			</div>
			<table class="table table-bordered small">
				<thead>
				<tr>
					<th>Реквизит</th>
					<th>До изменения</th>
					<th width="15%">После изменения</th>
					<th>Подтверждающие документы*</th>
				</tr>
				</thead>
				<tr>
					<td>Наименование банка</td>
					<td>Мастер-банк (ОАО) г. Москва</td>
					<td><input type="text" value="" name="val-1"></td>
					<td><input type="file"></td>
				</tr>
				<tr>
					<td>Расчетный счет</td>
					<td>4070802810500000100000000</td>
					<td><input type="text" value="" name="val-2"></td>
					<td><input type="file"></td>
				</tr>
				<tr>
					<td>Корр.счет</td>
					<td>30101810000000000353</td>
					<td><input type="text" value="" name="val-3"></td>
					<td><input type="file"></td>
				</tr>
				<tr>
					<td>БИК</td>
					<td>44525353</td>
					<td><input type="text" value="" name="val-4"></td>
					<td><input type="file"></td>
				</tr>
			</table>


			<div class="btn-group btn-group-justified" role="group">
				<a class="btn btn-default light-blue" role="button" id="generate_letter">Сформировать письмо <span class="glyphicon glyphicon-list-alt" area-hidden="true"></span></a>
				<a class="btn btn-success demo">Подписать <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span></a>
				<a class="btn btn-default demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
				<a class="btn btn-default demo">Отправить <span class="glyphicon glyphicon-send" aria-hidden="true"></span></a>
			</div>

		</form>

	</div>


	<script>
		$('#generate_letter').on('click', function () {
			var form = $(".is-form");
			$(form).submit();
		});

		//Динамика
		var org_rubrics = ['Организация', 'Банковские реквизиты', 'Адрес', 'Контакты'];
		var contract_rubrics = ['Точка подключения', 'Площадка'];


		$("#object").on("change", function () {
			if ($(this).val() == 'Организация') {
				$("#rubric").empty();
				$.each(org_rubrics, function (i, val) {
					$("#rubric").append('<option value="'+ val +'">' + val + '</option>')
				});
			} else {
				$("#rubric").empty();
				$.each(contract_rubrics, function (i, val) {
					$("#rubric").append('<option value="'+ val +'">' + val + '</option>')
				});
			}
		});

		$("#rubric").on("change", function () {
			$('.demo').click();
		});




	</script>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>