<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

	<div class="col-md-12" style="margin-top: 15px; margin-bottom: 20px">
		<h1 class="header">Список заявок</h1>
		<table class="table table-responsive">
			<thead>
			<tr>
				<th>Дата заявки</th>
				<th>Тип заявки</th>
				<th>Срок выполнения</th>
				<th>Статус</th>
				<th>Комментарий</th>
				<th>Действия</th>
			</tr>
			</thead>
			<tbody>
			<tr>
				<td>15.05.2016</td>
				<td>Заявка на заключение доп. соглашения</td>
				<td>01.06.2016</td>
				<td>Создана</td>
				<td></td>
				<td>
					<ul class="list-inline">
						<li><a href="javascript:void(0)" class="btn light-blue" data-toggle="tooltip" data-placement="top" title="Посмотреть"><span class="glyphicon glyphicon-search"></span></a></li>
						<li><a href="javascript:void(0)" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Удалить"><span class="glyphicon glyphicon-trash"></span></a></li>
						<li><a href="javascript:void(0)" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Сохранить"><span class="glyphicon glyphicon-save"></span></a></li>
					</ul>
				</td>
			</tr>
			<tr>
				<td>15.05.2016</td>
				<td>Заявка на исключение точки отбора газа из действующего ДПГ</td>
				<td>25.04.2016</td>
				<td>На рассмотрении</td>
				<td></td>
				<td>
					<ul class="list-inline">
						<li><a href="javascript:void(0)" class="btn light-blue" data-toggle="tooltip" data-placement="top" title="Посмотреть"><span class="glyphicon glyphicon-search"></span></a></li>
					</ul>
				</td>
			</tr>
			<tr>
				<td>15.05.2016</td>
				<td>Заявка на изменение объемов поставки газа</td>
				<td>25.04.2016</td>
				<td>Подписана</td>
				<td></td>
				<td>
					<ul class="list-inline">
						<li><a href="javascript:void(0)" class="btn light-blue" data-toggle="tooltip" data-placement="top" title="Посмотреть"><span class="glyphicon glyphicon-search"></span></a></li>
						<li><a href="javascript:void(0)" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Удалить"><span class="glyphicon glyphicon-trash"></span></a></li>
						<li><a href="javascript:void(0)" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Сохранить"><span class="glyphicon glyphicon-save"></span></a></li>
					</ul>
				</td>
			</tr>



			</tbody>
		</table>
		<a href="/main/"><span class="glyphicon glyphicon-chevron-left"></span>Назад</a>
	</div>


<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>