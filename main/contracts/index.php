<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px; margin-bottom: 20px">
		<h1 class="header">Договора <span class="pull-right btn btn-sm btn-default"><a href="javascript:void(0)" id="changeLayout">Список/Карточки</a></span></h1>
		<div class="row" id="cardLayout">
			<div style="overflow: hidden;"><!--Договор 1-->
				<div class="col-md-6 card card-wrap">
					<h2 class="header"><a href="detail.php">№ 61575 от 02.01.2016</a> <span class="pull-right"><small class="text-success">Действующий</small></span></h2>
					<form action="#" class="">
						<div class="form-group">
							<label for="summ">Сумма договора</label>
							<div class="input-group">
								<input type="text" class="form-control" id="summ" aria-label="Сумма договра" value="2 000 000" disabled>
								<span class="input-group-addon">тыс. <i class="glyphicon glyphicon-ruble"></i></span>
							</div>
						</div>
						<div class="form-group">
							<label for="debt">Задолженность на текущую дату</label>
							<div class="input-group">
								<span class="input-group-addon">-</span>
								<input type="text" class="form-control alert-danger" id="debt" aria-label="Долг" value="300 000" disabled>
								<span class="input-group-addon">тыс. <i class="glyphicon glyphicon-ruble"></i></span>
							</div>
						</div>
						<div class="form-group">
							<label for="debt">Общий объем</label>
							<div class="input-group">
								<input type="text" class="form-control" id="debt" aria-label="Общий объем" value="1 200" disabled>
								<span class="input-group-addon">куб.м.</span>
							</div>
						</div>

						<div class="card">
							<h3 class="header header3">Неподписанные документы</h3>
							<ul class="list-unstyled">
								<li><span class="proplems-name"><a href="#" class="demo">Доп. соглашения</a></span> <span class="small text-danger pull-right">не подписан</span></li>
								<li><span class="proplems-name"><a href="#" class="demo">Акт реализации</a></span> <span class="small text-danger pull-right">не подписан</span></li>
							</ul>
						</div>
						<br>

						<h3 class="header header3">Точки подключения</h3>
						<ul class="list-unstyled">
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №16 п.Зирган </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №19 с.Нордовка </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
						<span class="hide" id="tp1">
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
						</span>
							<li><a href="javascript:void(0)" id="tp_more">еще...</a></li>
						</ul>
					</form>
				</div>
				<!--Договор 2-->
				<div class="col-md-6 card card-wrap">
					<h2 class="header"><a href="detail.php">№ 61781 от 02.03.2016</a> <span class="pull-right"><small class="text-danger">Завершенный</small></span></h2>
					<form action="#" class="">
						<div class="form-group">
							<label for="summ">Сумма договора</label>
							<div class="input-group">
								<input type="text" class="form-control" id="summ" aria-label="Сумма договра" value="5 000 000" disabled>
								<span class="input-group-addon">тыс. <i class="glyphicon glyphicon-ruble"></i></span>
							</div>
						</div>
						<div class="form-group">
							<label for="debt">Задолженность на текущую дату</label>
							<div class="input-group">
								<input type="text" class="form-control alert-info" id="debt" aria-label="Долг" value="500 000" disabled>
								<span class="input-group-addon">тыс. <i class="glyphicon glyphicon-ruble"></i></span>
							</div>
						</div>
						<div class="form-group">
							<label for="debt">Общий объем</label>
							<div class="input-group">
								<input type="text" class="form-control" id="debt" aria-label="Общий объем" value="3 000" disabled>
								<span class="input-group-addon">куб.м.</span>
							</div>
						</div>

						<div class="card">
							<h3 class="header header3">Неподписанные документы</h3>
							<ul class="list-unstyled">
								<li><span class="proplems-name"><a href="#" class="demo">Доп. соглашения</a></span> <span class="small text-danger pull-right">не подписан</span></li>
								<li><span class="proplems-name"><a href="#" class="demo">Акт реализации</a></span> <span class="small text-danger pull-right">не подписан</span></li>
								<li><span class="proplems-name"><a href="#" class="demo">Акт сверки</a></span> <span class="small text-danger pull-right">не подписан</span></li>
							</ul>
						</div>
						<br>

						<h3 class="header header3">Точки подключения</h3>
						<ul class="list-unstyled">
							<li><span><a href="#" class="demo">Котельная №18 c.Антоновка</a></span></li>
							<li><span><a href="#" class="demo">Котельная №12 с.Васильевка </a></span></li>
							<li><span><a href="#" class="demo">Котельная №13 д.Дарьино </a></span></li>
							<li><a href="#" class="demo">еще...</a></li>
						</ul>
					</form>
				</div>
			</div>
		</div>

		<div class="row hidden" id="tableLayout">
			<table class="table table-bordered">
				<thead>
				<tr>
					<th>№ Договора и дата</th>
					<th>Статус</th>
					<th>Сумма договора, руб</th>
					<th>Задолженность на текущую дату</th>
					<th>Общий объем, куб.м.</th>
					<th>Проблемы договора</th>
					<th>Точки подключения</th>
				</tr>
				</thead>
				<tr>
					<td rowspan="3"><a href="detail.php">№ 61575 от 02.01.2016</a></td>
					<td rowspan="3">Действующий</td>
					<td rowspan="3">2 000 000</td>
					<td rowspan="3">-300 000</td>
					<td rowspan="3">1 200</td>
					<td>Доп. соглашения - <span class="text-danger">не подписан</span></td>
					<td><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №16 п.Зирган </a></td>
				</tr>
				<tr>
					<td>Акт реализации - <span class="text-danger">не подписан</span></td>
					<td><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №19 с.Нордовка </a></td>
				</tr>
				<tr>
					<td></td>
					<td><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></td>
				</tr>

				<tr>
					<td rowspan="3"><a href="detail.php">№ 61781 от 02.03.2016</a></td>
					<td rowspan="3">Завершенный</td>
					<td rowspan="3">5 000 000</td>
					<td rowspan="3">500 000</td>
					<td rowspan="3">3 000</td>
					<td>Доп. соглашения -
						<div class="text-danger">не подписан</div>
					</td>
					<td><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №18 c.Антоновка</a></td>
				</tr>
				<tr>
					<td>Акт реализации -
						<div class="text-danger">не подписан</div>
					</td>
					<td><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №12 с.Васильевка</a></td>
				</tr>
				<tr>
					<td>Акт сверки -
						<div class="text-danger">не подписан</div>
					</td>
					<td><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №13 д.Дарьино</a></td>
				</tr>
			</table>
		</div>

	</div>

	<script>
		$('#changeLayout').on('click', function () {
			var cardLayout = $('#cardLayout');
			var tableLayout = $('#tableLayout');

			if (cardLayout.hasClass('hidden')) {
				cardLayout.removeClass('hidden');
				tableLayout.addClass('hidden');
			} else {
				cardLayout.addClass('hidden');
				tableLayout.removeClass('hidden');
			}
		})

		$('#tp_more').on('click', function(){
			$('#tp1').toggleClass('hide');
			if($('#tp1').hasClass('hide')){
				$(this).text('еще...')
			}else{
				$(this).text('свернуть...')
			}
		})
	</script>

<? require_once "modal_tp.php" ?>
<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>