<div class="modal fade tp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="current">
			<div class="modal-header blue"><h1 class="header">Котельная №16 п.Зирган - № 61576 от 02.01.2016</h1></div>
			<div class="modal-body">
				<form action="#" class="">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="address">Адрес</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
									<input type="text" class="form-control" id="address" name="address" aria-label="Адрес" value="Поселок Зирган, ул Мира 56">
								</div>
							</div>
							<div class="form-group">
								<label for="grs">Вид потребления</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-scale"></i></span>
									<select class="form-control" id="grs">
										<option value="">По регулируемой цене</option>
										<option value="">По нерегулируемой цене</option>
									</select>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="form-group">
								<label for="grs">ГРС</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="glyphicon glyphicon-transfer"></i></span>
									<select class="form-control" id="grs">
										<option value="">ГРС Мелеуз</option>
										<option value="">ГРС Кумертау</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="address">Общий объем</label>
								<div class="input-group">
									<input type="text" class="form-control" id="volume" value="26949">
									<span class="input-group-addon">тыс.куб.м.</span>
								</div>
							</div>
						</div>
					</div>

					<ul class="nav nav-tabs" role="tablist">
						<li role="ploshadka" class="active"><a href="#ploshadka" aria-controls="ploshadka" role="tab" data-toggle="tab">Площадки</a></li>
					</ul>

					<!-- Tab panes -->
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="ploshadka">
							<div class="table-responsive">
								<br><br>
								<a href="/main/services/add_point.php" class="btn light-blue"><i class="glyphicon glyphicon-plus"></i> Добавить</a>
								<a href="/main/services/delete_point.php" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Удалить</a>
								<br><br>
								<table class="table table-bordered" id="ploshadki">
									<thead>
									<tr>
										<th rowspan="2">Наименование</th>
										<th rowspan="2">Адрес</th>
										<th rowspan="2">Наменование оборудования</th>
										<th rowspan="2">Категория потребления</th>
										<th rowspan="2">Группа потребления</th>
										<th rowspan="2">Количество, шт</th>
										<th rowspan="2">Установленная мощность, н.м. куб/час</th>
										<th rowspan="2">Средства измернений входящие в состав узла учета газа</th>
										<th rowspan="2">Годовой объем, тыс.куб.м.</th>
										<th colspan="3">Тарифы без НДС, руб. за тыс.куб.м.</th>
										<th rowspan="2">Итого, без НДС, руб.</th>
										<th rowspan="2">НДС, руб.</th>
										<th rowspan="2">Стоимость поставки газа, ИТОГО с НДС, руб.</th>
										<th rowspan="2">Передача данных по телеметрии</th>
										<th rowspan="2"></th>
									</tr>
									<tr>
										<th>Газ</th>
										<th>ПССУ</th>
										<th>ТТР</th>
									</tr>
									</thead>
									<tbody id="props_body">
									<tr>
										<td>Котельная №16 п.Зирган</td>
										<td>п.Зирган</td>
										<td>Котел Д721 Г-Ф</td>
										<td>Кроме населения</td>
										<td>5-я группа (От 0,1 до 1 включительно)</td>
										<td>3</td>
										<td>66</td>
										<td>БИП ИРВИС-РС4 Датчик давления ДМР-331 Датчик температуры ТПТ-17-1 ПП ИРВИС-РС-4 Ду 50</td>
										<td>573,22</td>
										<td>3 799,00</td>
										<td>167,93</td>
										<td>682,13</td>
										<td>2 664 934,17</td>
										<td>479 688,15</td>
										<td>3 144 622,32</td>
										<td class="text-center"><input type="checkbox" value="telemetry" name="telemetry"></td>
										<td><a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target=".monthly">Разбивка </br> объемов по </br> месяцам</a></td>
									</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>


				</form>
			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn blue">ОК</button>
				<button data-dismiss="modal" class="btn btn-danger">Отмена</button>
			</div>
		</div>

	</div>
</div>


<div class="modal fade monthly" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div class="modal-dialog modal-lg">
		<div class="modal-content" id="current">
			<div class="modal-header blue"><h1 class="header">Разбивка объемов по месяцам</h1></div>
			<div class="modal-body">
				<form action="#" class="">
					<div class="table-responsive">
						<table class="table table-bordered" id="ploshadki">
							<thead>
							<tr>
								<th>Общий, куб.м</th>
								<th>Январь</th>
								<th>Февраль</th>
								<th>Март</th>
								<th>Апрель</th>
								<th>Май</th>
								<th>Июнь</th>
								<th>Июль</th>
								<th>Август</th>
								<th>Сентябрь</th>
								<th>Октябрь</th>
								<th>Ноябрь</th>
								<th>Декабрь</th>
							</tr>
							</thead>
							<tr>
								<td></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
								<td><input type="text"></td>
							</tr>

						</table>
					</div>



				</form>

			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" class="btn blue">ОК</button>
				<button data-dismiss="modal" class="btn btn-danger">Отмена</button>
				<a href="/main/services/change_volume.php"  class="btn btn-info pull-right">Изменить</a>
			</div>
		</div>

	</div>
</div>