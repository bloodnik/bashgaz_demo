<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

<? require_once "left_menu.php" ?>

	<div class="col-md-9 detail_contract" style="margin-top: 15px; margin-bottom: 20px">
		<h1 class="header">№ 61757 от 02.01.2016 <span class="pull-right small text-success">Действующий</span></h1>
		<div class="row">
			<div class="card">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="summ">Сумма договора</label>
							<div class="input-group">
								<input type="text" class="form-control" id="summ" aria-label="Сумма договра" value="300 000">
								<span class="input-group-addon">тыс. <i class="glyphicon glyphicon-ruble"></i></span>
							</div>
						</div>
						<div class="form-group">
							<label for="summ">Задолженность</label>
							<div class="input-group">
								<input type="text" class="form-control" id="summ" aria-label="" value="5605">
								<span class="input-group-addon">тыс. <i class="glyphicon glyphicon-ruble"></i></span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="summ">Схема оплаты</label>
							<input type="text" class="form-control" id="summ" value="">
						</div>
						<div class="form-group">
							<label for="summ">Общий объем</label>
							<div class="input-group">
								<input type="text" class="form-control" id="summ" aria-label="" value="5605">
								<span class="input-group-addon">куб.м.</span>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<button class="demo btn light-blue">Справка о задолженности</button>
					</div>
				</div>
			</div>
		</div>
		<br>


		<div class="row">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#docs" aria-controls="docs" role="tab" data-toggle="tab">Документы</a></li>
			</ul>
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane active" id="docs">
					<div class="col-md-12">
						<br>
						<form class="form-inline">
							<div class="form-group">
								<label for="exampleInputName2">От </label>
								<select class="form-control">
									<option>Январь</option>
									<option>Февраль</option>
									<option>Март</option>
									<option>Апрель</option>
									<option>Май</option>
									<option>...</option>
								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputEmail2"> До </label>
								<select class="form-control">
									<option>Январь</option>
									<option>Февраль</option>
									<option selected>Март</option>
									<option>Апрель</option>
									<option>Май</option>
									<option>...</option>
								</select>
							</div>
							<button type="button" class="demo btn light-blue">Сформировать</button>
							<br>
							<br>
						</form>

						<div class="col-md-3">
							<div class="row">
								<div class="card">
									<ul class="list-unstyled">
										<li><a href="javascript:void(0)">Акт сверки</a></li>
										<li><a href="javascript:void(0)">Акт реализации</a></li>
										<li><a href="javascript:void(0)">Доп соглашение</a></li>
										<li><a href="javascript:void(0)">Судебные иски/претензии</a></li>
									</ul>
								</div>
							</div>
						</div>

						<div class="col-md-9">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th>Документ по договору</th>
									<th>Дата</th>
									<th>Статус</th>
								</tr>
								</thead>
								<tr>
									<td>Акт сверки № 65148</td>
									<td>15.01.2016</td>
									<td><span class="text-success">Подписан</span></td>
								</tr>
								<tr>
									<td>Акт сверки № 56498</td>
									<td>17.01.2016</td>
									<td><span class="text-danger">Не подписан</span></td>
								</tr>
								<tr>
									<td>Акт сверки № 67949</td>
									<td>02.01.2016</td>
									<td><span class="text-danger">Не подписан</span></td>
								</tr>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row card-group">
			<div class="col-md-6 card-wrap ">
				<div class="row">
					<div class="card">
						<h3 class="header header3">Точки подключения (площадки)</h3>
						<ul class="list-unstyled">
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №16 п.Зирган </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №19 с.Нордовка </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
						<span class="hide" id="tp1">
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
							<li><span><a href="#" role="button" data-toggle="modal" data-target=".tp">Котельная №23 с.Юлдаш </a></span></li>
						</span>
							<li><a href="javascript:void(0)" id="tp_more">еще...</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6 ">
				<div class="row">
					<div class="card">
						<h3 class="header header3">Неподписанные документы</h3>
						<ul class="list-unstyled">
							<li><span class="proplems-name"><a href="#">Доп. соглашения</a></span> <span class="text-danger pull-right">не подписан</span></li>
							<li><span class="proplems-name"><a href="#">Акт реализации</a></span> <span class="text-danger pull-right">не подписан</span></li>
							<li><span class="proplems-name"><a href="#">Акт сверки</a></span> <span class="text-danger pull-right">не подписан</span></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		$('#tp_more').on('click', function(){
			$('#tp1').toggleClass('hide');
			if($('#tp1').hasClass('hide')){
				$(this).text('еще...')
			}else{
				$(this).text('свернуть...')
			}
		})
	</script>

<? require_once "modal_tp.php" ?>
<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>