<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Включение точки отбора</title>

	<!--Стили-->
	<link rel="stylesheet" href="/js/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/js/jasny-bootstrap/css/jasny-bootstrap.min.css"/>

	<link rel="stylesheet" href="/css/colors.css"/>
	<link rel="stylesheet" href="/css/style.css"/>

	<!--Скрипты-->
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/jasny-bootstrap/js/jasny-bootstrap.js"></script>
</head>
<body>

<div class="container">

	<p class="text-right">Приложение №6</p>
	<p class="text-center col-md-12" style="text-decoration: underline">Письмо о включении точки отбора в действующий договор (контракт)</p>
	<p class="text-center col-md-12" >Фирменный бланк</p>
	<p class="text-left col-md-3 col-lg-offset-9" style="font-weight: bold">
		Заместителю генерального директора по реализации газа ООО "Газпром межрегионгаз Уфа" <br/>
		Р.Р. Ахмадееву
	</p>
	<br/><br/>
	<p class="text-center col-md-12" style="font-weight: bold">Уважаемый Рустем Рамильевич!</p><br/>

	<p>В связи с <span style="text-decoration: underline"><?=$_REQUEST["cause"]?></span>, расположенного по адресу:<span style="text-decoration: underline"><?=$_REQUEST["address"]?></span>
        просим Вас включить точку отбора газа в договор(контракт) поставки газа № <span style="text-decoration: underline"><?=$_REQUEST["current_contract"]?></span>
        с договора (контракта)№<span style="text-decoration: underline"><?=$_REQUEST["current_contract"]?></span>, с <span style="text-decoration: underline">ООО "Пример"</span>
        начиная с ___.___.______.</p>
	<p>В соответствии с утвежденным планом финансово-хозяйственной деятельности и в пределах утвежденного муниципального задания в 2016 году из общей суммы субсидий, выделенной на выполнение муниципального задания
		, сумма на оплату природного газа составляет____рублей. Оплату денежными средствами за поставку природного газа по договору гарантируем</p>
	<p><span style="text-decoration: underline"><?=$_REQUEST["org"]?></span>, сообщает, что в качестве резервного(аварийного) топлива для отопления(производственных нужд) <span style="text-decoration:
	underline"><?=$_REQUEST["tp_name"]?></span>
        находящегося по адресу:<span style="text-decoration: underline"><?=$_REQUEST["address"]?></span>,
		<span style="text-decoration: underline"><?=$_REQUEST["rezerv"]?></span> и в случае аварийного или
	планового(по предварительному уведомлению) прекращения подачи газа к ООО "Газпром межрегионгаз УФа" предъявлять не будет</p>
	<br/>
	<p>Приложение: <br/>
		1) Заявка на поставку газа с приложениями</p><br/>
		2) Копии документов, потверждающих право собсвенности на газопотребляющий объект
		3) Копия акта приема - передачи оборудования или иной документ по передаче объекта
	<br/>
	<br/>


	<p class="col-md-6" style="font-weight: bold">Должность</p>
	<p class="col-md-6 text-right" style="font-weight: bold">подпись, ФИО</p>

	<br/><br/>
	<p class="" >Исполнитель: ФИО, телефон</p>


</div>



</body>
</html>