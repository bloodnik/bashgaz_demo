<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Письмо о расторжении при ликвидации</title>

	<!--Стили-->
	<link rel="stylesheet" href="/js/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/js/jasny-bootstrap/css/jasny-bootstrap.min.css"/>

	<link rel="stylesheet" href="/css/colors.css"/>
	<link rel="stylesheet" href="/css/style.css"/>

	<!--Скрипты-->
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/jasny-bootstrap/js/jasny-bootstrap.js"></script>
</head>
<body>

<div class="container">

	<p class="text-right">Приложение №2</p>
	<p class="text-center col-md-12" style="text-decoration: underline">Письмо о расторжении при ликвидации</p>
	<p class="text-center col-md-12">Фирменный бланк(Наименование, почтовый адрес, телефон, e-mail)</p>
	<p class="text-left col-md-3 col-lg-offset-9" style="font-weight: bold">
		Заместителю генерального директора по реализации газа ООО "Газпром межрегионгаз Уфа" <br/>
		Р.Р. Ахмадееву
	</p>
	<br/><br/>
	<p class="text-center col-md-12" style="font-weight: bold">Уважаемый Рустем Рамильевич!</p><br/>

	<p>В связи с <span style="text-decoration: underline">ликвидацией</span>, расположенного по адресу:
		<span style="text-decoration: underline;"><?=$_REQUEST["address"]?></span> просим Вас расторгнуть договор (контракт)
		поставки газа <span class="dashed_underline"><?=$_REQUEST["current_contract"]?></span></span>, начиная с
		<span class="dashed_underline">Наименование организации</span>, начиная с <span class="dashed_underline"><?=$_REQUEST["start"]?></span>.
	</p>
		Приложение: Копия акта об установлении заглушки, выданного ОАО "Газпром газораспределение Уфа" или копия акта опломбирования выданного ООО
		"Газпром межрегионгаз Уфа".
	<p>

	<br/>
	<br/>


	<p class="col-md-6" style="font-weight: bold">Должность</p>
	<p class="col-md-6 text-right" style="font-weight: bold">подпись, ФИО</p>

	<br/><br/>
	<p class="">Исполнитель: ФИО, телефон</p>


</div>


</body>
</html>