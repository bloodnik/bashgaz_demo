<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Письмо о снижении объемов по договору (контракту)</title>

	<!--Стили-->
	<link rel="stylesheet" href="/js/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/js/jasny-bootstrap/css/jasny-bootstrap.min.css"/>

	<link rel="stylesheet" href="/css/colors.css"/>
	<link rel="stylesheet" href="/css/style.css"/>

	<!--Скрипты-->
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/jasny-bootstrap/js/jasny-bootstrap.js"></script>
</head>
<body>

<div class="container">

	<p class="text-right">Приложение №7</p>
	<p class="text-center col-md-12" style="text-decoration: underline">Письмо о снижении объемов по договору (контракту)</p>
	<p class="text-center col-md-12">Фирменный бланк(Наименование, почтовый адрес, телефон, e-mail)</p>
	<p class="text-left col-md-3 col-lg-offset-9" style="font-weight: bold">
		Заместителю генерального директора по реализации газа ООО "Газпром межрегионгаз Уфа" <br/>
		Р.Р. Ахмадееву
	</p>
	<br/><br/>
	<p class="text-center col-md-12" style="font-weight: bold">Уважаемый Рустем Рамильевич!</p><br/>
	<span class="dashed_underline"><?=$_REQUEST["address"]?></span>
	<p>
		В связи со снижением денежных средств в размере (<span class="dashed_underline"><?=$_REQUEST["limit"]?></span>) просим Вас
		уменьшить лимиты (объемы) газа, согласно договора(контаркта) поставки газа <span class="dashed_underline"><?=$_REQUEST["contract"]?></span>
		<span class="dashed_underline">наименование организации</span> до объема <span class="dashed_underline"><?=$_REQUEST["volume"]?></span> тыс.куб.м
		начиная с <span class="dashed_underline"><?=$_REQUEST["date"]?></span> согласно прилагаемой заявке.
	</p>
	<p>
		В соответствии с утвержденным планом финансово-хозяйственной деятельности и в пределах утрвежденного мунициапального задания в 2016 году из общей
		суммы субсидий, веделенной на выполнение муниципального задания, сумма на оплату природного газа составляет
		<span class="dashed_underline"><?=$_REQUEST["budjet"]?></span> рублей. Оплату денеждными средствами за поставку природного газа по договору
		гарантируем.
	</p>
	<p>
		Приложение: Заявка на поставку газа с приложениями в соответствии с выделенной суммой денег.
	<p>

		<br/>
		<br/>


	<p class="col-md-6" style="font-weight: bold">Должность</p>
	<p class="col-md-6 text-right" style="font-weight: bold">подпись, ФИО</p>

	<br/><br/>
	<p class="">Исполнитель: ФИО, телефон</p>


</div>


</body>
</html>