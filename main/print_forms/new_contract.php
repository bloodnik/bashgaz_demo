<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Письмо о заключении нового договора (контракта) на поставку газа</title>

	<!--Стили-->
	<link rel="stylesheet" href="/js/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/js/jasny-bootstrap/css/jasny-bootstrap.min.css"/>

	<link rel="stylesheet" href="/css/colors.css"/>
	<link rel="stylesheet" href="/css/style.css"/>

	<!--Скрипты-->
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/jasny-bootstrap/js/jasny-bootstrap.js"></script>
</head>
<body>

<div class="container">

	<p class="text-right">Приложение №1</p>
	<p class="text-center col-md-12" style="text-decoration: underline">Письмо о заключении нового договора (контракта) на поставку газа</p>
	<p class="text-center col-md-12">Фирменный бланк(Наименование, почтовый адрес, телефон, e-mail)</p>
	<p class="text-left col-md-3 col-lg-offset-9" style="font-weight: bold">
		Заместителю генерального директора по реализации газа ООО "Газпром межрегионгаз Уфа" <br/>
		Р.Р. Ахмадееву
	</p>
	<br/><br/>
	<p class="text-center col-md-12" style="font-weight: bold">Уважаемый Рустем Рамильевич!</p><br/>

	<p>В связи с газификацией <span style="text-decoration: underline"><?=$_REQUEST["name"]?></span>, расположенного по адресу:
		<span style="text-decoration: underline;"><?=$_REQUEST["address"]?></span> просим Вас заключить договор (контракт)
		поставки газа с <span class="dashed_underline">Наименование организации</span>, начиная с
		<span class="dashed_underline"><?=$_REQUEST["start"]?></span> в объеме
		<span class="dashed_underline"><?=$_REQUEST["volume"]?></span> тыс. куб. м в год.
	</p>

	<p>
		В соответствии с утвержденным планом финансово-хозяйственной деятельности и в пределах утвержденного
		муниципального задания в 2016 году из общей суммы субсидий, выделенной на выполнение муниципального задания, сумма
		на оплату природного газа составляет <span class="dashed_underline"><?=$_REQUEST["budjet"]?></span>
		рублей. Оплату денежными средствами за поставку природного газа по договору гарантируем.
	</p>
	<p>
		<span class="dashed_underline"> Наименование организации</span>, сообщает, что в качестве резервного(аварийного) топлива для отопления
		(производственных нужд) <span class="dashed_underline"><?=$_REQUEST["name"]?></span>
		находящегося по адресу: <span style="text-decoration: underline;"><?=$_REQUEST["address"]?></span>
		, предусмотрено <span style="text-decoration: underline;"><?=$_REQUEST["rezerv_fuel"]?></span>
		(не предусмотрено) и в случае аварийного или планового (по предварительному уведомлению) прекращения подачи
		газа притензий к ООО "Газпром межрегионгаз Уфа" предъявлять не будет.
	</p>


	<br/>
	<br/>


	<p class="col-md-6" style="font-weight: bold">Должность</p>
	<p class="col-md-6 text-right" style="font-weight: bold">подпись, ФИО</p>

	<br/><br/>
	<p class="">Исполнитель: ФИО, телефон</p>


</div>


</body>
</html>