<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Исключение точки отбора</title>

	<!--Стили-->
	<link rel="stylesheet" href="js/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="js/jasny-bootstrap/css/jasny-bootstrap.min.css"/>

	<link rel="stylesheet" href="css/colors.css"/>
	<link rel="stylesheet" href="css/style.css"/>

	<!--Скрипты-->
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="js/jasny-bootstrap/js/jasny-bootstrap.js"></script>
</head>
<body>

<div class="container">

    <?
        $cause = "";
        switch($_REQUEST["tp_delete_cause"]){
            case 1:
                $cause = "ликвидацией юридического лица";
                break;
            case 2:
                $cause = "реорганизацией юридического лица";
                break;
        }

    ?>


	<p class="text-right">Приложение №4</p>
	<p class="text-center col-md-12" style="text-decoration: underline">Письмо об исключении точки отбора из действующего договора(контракта)при ликвидации</p>
	<p class="text-center col-md-12" >Фирменный бланк</p>
	<p class="text-left col-md-3 col-lg-offset-9" style="font-weight: bold">
		Заместителю генерального директора по реализации газа ООО "Газпром межрегионгаз Уфа" <br/>
		Р.Ф. Ахмадееву
	</p>
	<br/><br/>
	<p class="text-center col-md-12" style="font-weight: bold">Уважаемый Рамиль Фаатович!</p><br/>

	<p>В связи с <span style="text-decoration: underline"><?=$cause ?></span>, расположенного по адресу:<span style="text-decoration: underline"><?= $_REQUEST["delete_address"] ?></span>
        просим Вас исключить точку отбора газа <span style="text-decoration: underline">Точка <?= $_REQUEST["tp_delete_select"] ?></span>
        из договора (контракта) поставки газа<span style="text-decoration: underline"><?= $_REQUEST["delete_contract"] ?></span>, начиная с <span style="text-decoration: underline"><?= $_REQUEST["delete_date"] ?></span></p>
	<br/>
	<p>Приложение: Копия акта заглушки, выданного ОАО "Газпром газораспределение Уфа" или копия акта опломбирования, выданного ООО "Газпром межрегионгаз Уфа"</p>
	<br/>
	<br/>


	<p class="col-md-6" style="font-weight: bold">Должность</p>
	<p class="col-md-6 text-right" style="font-weight: bold">подпись, ФИО</p>

	<br/><br/>
	<p class="" >Исполнитель: ФИО, телефон</p>


</div>



</body>
</html>