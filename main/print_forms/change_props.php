<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title>Письмо об изменении реквизитов</title>

	<!--Стили-->
	<link rel="stylesheet" href="/js/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="/js/jasny-bootstrap/css/jasny-bootstrap.min.css"/>

	<link rel="stylesheet" href="/css/colors.css"/>
	<link rel="stylesheet" href="/css/style.css"/>

	<!--Скрипты-->
	<script type="text/javascript" src="/js/jquery.js"></script>
	<script type="text/javascript" src="/js/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="/js/jasny-bootstrap/js/jasny-bootstrap.js"></script>
</head>
<body>

<div class="container">

	<p class="text-right">Приложение №10</p>
	<p class="text-center col-md-12" style="text-decoration: underline">Письмо об изменении реквизитов</p>
	<p class="text-center col-md-12">Фирменный бланк(Наименование, почтовый адрес, телефон, e-mail)</p>
	<p class="text-left col-md-3 col-lg-offset-9" style="font-weight: bold">
		Заместителю генерального директора по реализации газа ООО "Газпром межрегионгаз Уфа" <br/>
		Р.Р. Ахмадееву
	</p>
	<br/><br/>
	<p class="text-center col-md-12" style="font-weight: bold">Уважаемый Рустем Рамильевич!</p><br/>
	<span class="dashed_underline"><?=$_REQUEST["address"]?></span>
	<p>
		В связи с <span class="dashed_underline"><?=$_REQUEST["cause"]?></span> просим Вас изменить следующие реквизиты по договору
		<span class="dashed_underline">номер договора</span>:
	</p>
	<p><span class="dashed_underline">Наименование банка</span>: <?=$_REQUEST['val-1']?></p>
	<p><span class="dashed_underline">Расчетный счет</span>: <?=$_REQUEST['val-2']?></p>
	<p><span class="dashed_underline">Корр.счет</span>: <?=$_REQUEST['val-3']?></p>
	<p><span class="dashed_underline">БИК</span>: <?=$_REQUEST['val-4']?></p>

	<p>
		Приложение: Подтверждающие документы по изменению реквизитов.
	<p>

		<br/>
		<br/>


	<p class="col-md-6" style="font-weight: bold">Должность</p>
	<p class="col-md-6 text-right" style="font-weight: bold">подпись, ФИО</p>

	<br/><br/>
	<p class="">Исполнитель: ФИО, телефон</p>


</div>


</body>
</html>