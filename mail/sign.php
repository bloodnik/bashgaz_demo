<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<div class="row">
			<h1 class="header">Настройка подписи</h1>

			<form action="#" class="form-inline">
				<button class="btn btn-success"><i class="glyphicon glyphicon-save"></i> Сохранить</button>
				<button class="btn btn-danger"><i class="glyphicon glyphicon-ban-circle"></i> Отмена</button>
			</form>
			<div class="clerafix"></div>
			<hr>

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-md-12">
						<form action="#" class="form-horizontal">
							<div class="form-group">
								<label>Сообщение</label>
								<textarea class="form-control" name="message" id="message" cols="30" rows="10"></textarea>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>