<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<div class="row">
			<h1 class="header">Написать письмо</h1>

			<form action="#" class="form-inline">
				<button class="btn btn-success demo"><i class="glyphicon glyphicon-send"></i> Отправить</button>
				<button class="btn btn-danger demo"><i class="glyphicon glyphicon-ban-circle"></i> Отмена</button>
			</form>
			<div class="clerafix"></div>

			<hr>

			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-md-12">
						<form action="#" class="form-horizontal">
							<div class="form-group">
								<label>Кому</label>
								<input type="text" class="form-control" id="addresse" name="addresse" placeholder="">
							</div>
							<div class="form-group">
								<label>Тема</label>
								<input type="text" class="form-control" id="subject" name="subject" placeholder="">
							</div>
							<div class="form-group">
								<label>Прикрепите файлы</label>
								<input type="file" id="attachment" name="attachment" placeholder="">
							</div>
							<div class="form-group">
								<label>Сообщение</label>
								<textarea class="form-control" name="message" id="message" cols="30" rows="10"></textarea>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>