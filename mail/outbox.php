<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<div class="row">
			<h1 class="header">Исходящие</h1>

			<form action="#" class="form-inline">
				<button class="btn btn-success demo"><i class="glyphicon glyphicon-open"></i> Открыть</button>
				<button class="btn btn-danger demo"><i class="glyphicon glyphicon-trash"></i> Удалить</button>
				<a href="new.php" class="btn light-blue"><i class="glyphicon glyphicon-envelope"></i> Написать письмо</a>
				<button class="btn light-info"
				        data-container="body"
				        data-toggle="popover"
				        data-trigger="focus"
				        data-placement="bottom"
				        title="Иванов Иван Иванович"
				        data-content="тел.: 8(347) 256-00-00">
					<i class="glyphicon glyphicon-earphone"></i> Позвонить куратору
				</button>
			</form>
			<div class="clerafix"></div>

			<hr>

			<table class="table table-bordered table-hover mail">
				<thead>
				<tr>
					<th></th>
					<th width="35%">Отправитель</th>
					<th>Тема</th>
					<th>Прикрепленные файлы</th>
					<th>Дата</th>
					<th>Статус</th>
				</tr>
				</thead>
				<tr>
					<td><input type="checkbox" value="1"></td>
					<td>[my@mail.ru] ООО "Пример"</td>
					<td>По поводу договора</td>
					<td><a href="#"><i class="glyphicon glyphicon-paperclip"></i> Договор № 134164</a></td>
					<td>02.07.2016</td>
					<td>Отправлено</td>
				</tr>
				<tr>
					<td><input type="checkbox" value="1"></td>
					<td>[my@mail.ru] ООО "Пример"</td>
					<td>Зачет оплаты</td>
					<td><a href="#"><i class="glyphicon glyphicon-paperclip"></i> Платежное поручение №1198919</a></td>
					<td>02.07.2016</td>
					<td>Доставлено</td>
				</tr>
				<tr>
					<td><input type="checkbox" value="1"></td>
					<td>[my@mail.ru] ООО "Пример"</td>
					<td>Зачет оплаты</td>
					<td><a href="#"><i class="glyphicon glyphicon-paperclip"></i> Платежное поручение №1198919</a></td>
					<td>26.07.2016</td>
					<td>Доставлено</td>
				</tr>
				<tr>
					<td><input type="checkbox" value="1"></td>
					<td>[my@mail.ru] ООО "Пример"</td>
					<td>Зачет оплаты</td>
					<td><a href="#"><i class="glyphicon glyphicon-paperclip"></i> Платежное поручение №1198919</a></td>
					<td>02.07.2016</td>
					<td>Прочитано</td>
				</tr>
				<tr>
					<td><input type="checkbox" value="1"></td>
					<td>[my@mail.ru] ООО "Пример"</td>
					<td>Зачет оплаты</td>
					<td><a href="#"><i class="glyphicon glyphicon-paperclip"></i> Платежное поручение №1198919</a></td>
					<td>02.07.2016</td>
					<td>Прочитано</td>
				</tr>
				<tr>
					<td><input type="checkbox" value="1"></td>
					<td>[my@mail.ru] ООО "Пример"</td>
					<td>Зачет оплаты</td>
					<td><a href="#"><i class="glyphicon glyphicon-paperclip"></i> Платежное поручение №1198919</a></td>
					<td>02.07.2016</td>
					<td>Прочитано</td>
				</tr>

			</table>

		</div>
	</div>
<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>