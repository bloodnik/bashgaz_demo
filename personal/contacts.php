<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header">Контактные данные</h1>

		<form class="is-form" method="get" action="#">
			<h3 class="header header3">Приемная</h3>

			<div class="form-group">
				<label>Телефон</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="" value="+73472464646">
			</div>
			<div class="form-group">
				<label>Сайт</label>
				<input type="text" class="form-control" id="acc-num" name="acc-num" placeholder="" value="romashka.ru">
			</div>
			<div class="form-group">
				<label>Факс</label>
				<input type="text" class="form-control" id="kor-num" name="kor-num" placeholder="" value="2464646">
			</div>

			<h3 class="header header3">Бухгалтерия</h3>

			<div class="form-group">
				<label>Контактное лицо</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="" value="Соколова М.П.">
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Телефон</label>
						<input type="text" class="form-control" id="inn" name="inn" aria-label="" value="+79279876543">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Факс</label>
						<input type="text" class="form-control" id="kpp" aria-label="" value="24569795">
					</div>
				</div>
			</div>

			<h3 class="header header3">Главный инженер</h3>

			<div class="form-group">
				<label>Контактное лицо</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="" value="Медведев Д.А.">
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Телефон</label>
						<input type="text" class="form-control" id="inn" name="inn" aria-label="" value="+798416549799">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Факс</label>
						<input type="text" class="form-control" id="kpp" aria-label="" value="26497954">
					</div>
				</div>
			</div>

			<h3 class="header header3">Ответственное лицо</h3>

			<div class="form-group">
				<label>Контактное лицо</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="" value="Малахов И.Н.">
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Телефон</label>
						<input type="text" class="form-control" id="inn" name="inn" aria-label="" value="+7984634946">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Факс</label>
						<input type="text" class="form-control" id="kpp" aria-label="" value="264979464">
					</div>
				</div>
			</div>

			<br>
			<div class="btn-group btn-group-justified" role="group">
				<a class="btn btn-success">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
			</div>
		</form>


	</div>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>