<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header">Адрес</h1>

		<form class="is-form" method="get" action="#">
			<h3 class="header header3">Юридический адрес</h3>
			<div class="form-group">
				<label>Регион</label>
				<input type="text" class="form-control" id="country" name="country" placeholder="" value="Башкортостан">
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>Город</label>
						<input type="text" class="form-control" id="city" data-kladr-type="city"  name="city" aria-label="" value="Уфа">
					</div>
					<div class="form-group">
						<label>Улица</label>
						<input type="text" class="form-control" id="street" name="street" value="Ленина">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>Индекс</label>
						<input type="text" class="form-control" id="index" name="index" aria-label="" value="450005">
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Дом</label>
								<input type="text" class="form-control" id="house" name="house" aria-label="" value="49">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Корпус</label>
								<input type="text" class="form-control" id="kor" name="kor" aria-label="" value="">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="checkbox">
				<label>
					<input type="checkbox" id="is_same" name="is_same" value="Y">
					Совпадает с юридическим
				</label>
			</div>

			<div id="fact">
				<h3 class="header header3">Фактический адрес</h3>
				<div class="form-group">
					<label>Страна</label>
					<input type="text" class="form-control" id="country" name="country" placeholder="" value="Россия">
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Город</label>
							<input type="text" class="form-control" id="city" name="city" aria-label="" value="Уфа">
						</div>
						<div class="form-group">
							<label>Улица</label>
							<input type="text" class="form-control" id="street" name="street" value="Ленина">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Индекс</label>
							<input type="text" class="form-control" id="index" name="index" aria-label="" value="450005">
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Дом</label>
									<input type="text" class="form-control" id="house" name="house" aria-label="" value="49">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Корпус</label>
									<input type="text" class="form-control" id="kor" name="kor" aria-label="" value="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="checkbox">
				<label>
					<input type="checkbox" id="is_same_post" name="is_same_post" value="Y">
					Совпадает с юридическим
				</label>
			</div>

			<div id="post">
				<h3 class="header header3">Почтовый адрес</h3>
				<div class="form-group">
					<label>Страна</label>
					<input type="text" class="form-control" id="country" name="country" placeholder="" value="Россия">
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Город</label>
							<input type="text" class="form-control" id="city" name="city" aria-label="" value="Уфа">
						</div>
						<div class="form-group">
							<label>Улица</label>
							<input type="text" class="form-control" id="street" name="street" value="Ленина">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Индекс</label>
							<input type="text" class="form-control" id="index" name="index" aria-label="" value="450005">
						</div>

						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Дом</label>
									<input type="text" class="form-control" id="house" name="house" aria-label="" value="49">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Корпус</label>
									<input type="text" class="form-control" id="kor" name="kor" aria-label="" value="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>


			<br>
			<div class="btn-group btn-group-justified" role="group">
				<a class="btn btn-success demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
				<a class="btn btn-default light-blue" href="director.php">Далее <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a>
			</div>
		</form>


	</div>

	<script>

		$("#is_same").on('change', function(){
			if($(this).prop("checked")) {
				$("#fact input").prop("disabled", "disabled");
			}else {
				$("#fact input").prop("disabled", "");
			}
		});
		$("#is_same_post").on('change', function(){
			if($(this).prop("checked")) {
				$("#post input").prop("disabled", "disabled");
			}else {
				$("#post input").prop("disabled", "");
			}
		});

	</script>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>