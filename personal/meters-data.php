<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>
<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<div class="row">
			<h1 class="header">Передать показания счетчиков</h1>

			<form action="#" class="form-inline">
				<div class="form-group">
					<label>№ договора*</label>
					<select name="current_contract" id="" class="form-control input-sm" required>
						<option value="№1354 от 20.04.2016">№1354 от 20.04.2016</option>
						<option value="№464 от 06.04.2016">№464 от 06.04.2016</option>
					</select>
				</div>
			</form>
			<div class="clerafix"></div>
			<hr>

			<table class="table table-bordered table-hover">
				<ul class="list-inline">
					<li>
						<button class="btn light-blue demo">Заполнить</button>
					</li>
					<li>
						<button class="btn btn-success demo"><i class="glyphicon glyphicon-plus"></i> Добавить</button>
					</li>
					<li></li>
					<button class="btn btn-danger demo"><i class="glyphicon glyphicon-trash"></i> Удалить</button>
					</li>
				</ul>
				<br><br>
				<thead>
				<tr>
					<th>Наименование площадки</th>
					<th>Текущие показания</th>
					<th>Дата снятия показателей</th>
					<th>Показания передаются по телеметрии</th>
				</tr>
				</thead>

				<tr>
					<td></td>
					<td><input type="text" class="form-control"/></td>
					<td><input type="text" class=" form-control datepicker"></td>
					<td>
						<ul class="list-inline text-right">
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Выгрузить форму"><i class="glyphicon
								glyphicon-download"></i> Выгрузить</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Прикрепить файл"><i class="glyphicon
								glyphicon-upload"></i> Прикпреить</a>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="text" class="form-control"/></td>
					<td><input type="text" class=" form-control datepicker"></td>
					<td>
						<ul class="list-inline text-right">
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Выгрузить форму"><i class="glyphicon
								glyphicon-download"></i> Выгрузить</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Прикрепить файл"><i class="glyphicon
								glyphicon-upload"></i> Прикпреить</a>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="text" class="form-control"/></td>
					<td><input type="text" class=" form-control datepicker"></td>
					<td>
						<ul class="list-inline text-right">
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Выгрузить форму"><i class="glyphicon
								glyphicon-download"></i> Выгрузить</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Прикрепить файл"><i class="glyphicon
								glyphicon-upload"></i> Прикпреить</a>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="text" class="form-control"/></td>
					<td><input type="text" class=" form-control datepicker"></td>
					<td>
						<ul class="list-inline text-right">
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Выгрузить форму"><i class="glyphicon
								glyphicon-download"></i> Выгрузить</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Прикрепить файл"><i class="glyphicon
								glyphicon-upload"></i> Прикпреить</a>
							</li>
						</ul>
					</td>
				</tr>
				<tr>
					<td></td>
					<td><input type="text" class="form-control"/></td>
					<td><input type="text" class=" form-control datepicker"></td>
					<td>
						<ul class="list-inline text-right">
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Выгрузить форму"><i class="glyphicon
								glyphicon-download"></i> Выгрузить</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="btn light-blue demo" data-toggle="tooltip" data-placement="top" title="Прикрепить файл"><i class="glyphicon
								glyphicon-upload"></i> Прикпреить</a>
							</li>
						</ul>
					</td>
				</tr>
			</table>

			<div class="pull-right">
				<button class="btn blue demo">Отправить</button>
			</div>
		</div>
	</div>

	<div class="clearfix"></div>

	<br><br><br>


<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>