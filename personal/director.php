<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<div class="row">
			<h1 class="header">Контакты</h1>
		</div>
		<div class="row">
			<form class="is-form" method="get" action="#">
				<div class="form-group">
					<label>Должность</label>
					<input type="text" class="form-control" id="pozition" name="pozition" placeholder="" value="Директор">
				</div>

				<div class="form-group">
					<label>ФИО</label>
					<input type="text" class="form-control" id="name" name="name" placeholder="" value="Иванов Иван Иванович">
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Телефон</label>
							<input type="text" class="form-control" id="phone" name="phone" aria-label="" value="+79871234567">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Факс</label>
							<input type="text" class="form-control" id="faks" aria-label="" value="234726484">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Мобильный телефон</label>
					<input type="tel" class="form-control" id="name" name="mobile" placeholder="" value="+79871234567">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="" value="ivanov@romashka.ru">
				</div>
			</form>
		</div>
		<div class="row">
			<br>
			<h2 class="header">Доверенности:</h2>
			<div class="col-md-6">
				<h3 class="header header2">Право подписи договора</h3>
				<div class="proxy">
					<p>Довернность №67 от 01.01.2016</p>
					<p>Период действия: 01.01.2016 - 31.12.2016</p>
					<form action="#" class="hidden">
						<div class="form-group">
							<label>Доверенность №</label>
							<input type="text" class="form-control" id="number" name="number" aria-label="" value="">
						</div>
						<label>Период</label>
						<div class="input-group">
							<input type="text" class="form-control input-sm datepicker" name="from">
							<span class="input-group-addon" id="sizing-addon2">-</span>
							<input type="text" class="form-control input-sm datepicker" name="to">
						</div>
					</form>
					<hr>
					<button class="btn blue btn-sm add-proxy">Добавить</button>
				</div>
			</div>
			<div class="col-md-6 proxy">
				<h3 class="header header2">Право подписи акта сверок</h3>
				<div class="proxy">
					<p>Довернность №70 от 01.01.2016</p>
					<p>Период действия: 01.01.2016 - 31.12.2016</p>
					<form action="#" class="hidden">
						<div class="form-group">
							<label>Доверенность №</label>
							<input type="text" class="form-control" id="number" name="number" aria-label="" value="">
						</div>
						<label>Период</label>
						<div class="input-group">
							<input type="text" class="form-control input-sm datepicker" name="from">
							<span class="input-group-addon" id="sizing-addon2">-</span>
							<input type="text" class="form-control input-sm datepicker" name="to">
						</div>
					</form>
					<hr>
					<button class="btn blue btn-sm add-proxy">Добавить</button>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<form class="is-form" method="get" action="#">
				<div class="form-group">
					<label>Должность</label>
					<input type="text" class="form-control" id="pozition" name="pozition" placeholder="" value="Зам директора">
				</div>
				<div class="form-group">
					<label>ФИО</label>
					<input type="text" class="form-control" id="name" name="name" placeholder="" value="Петров Петр Петрович">
				</div>

				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label>Телефон</label>
							<input type="text" class="form-control" id="phone" name="phone" aria-label="" value="+79876459785">
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label>Факс</label>
							<input type="text" class="form-control" id="faks" aria-label="" value="24967949">
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Мобильный телефон</label>
					<input type="tel" class="form-control" id="name" name="mobile" placeholder="" value="+79879784621">
				</div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" class="form-control" id="email" name="email" placeholder="" value="petrov@romashka.ru">
				</div>
			</form>
		</div>
		<div class="row">
			<br>
			<h2 class="header">Доверенности:</h2>
			<div class="col-md-6">
				<h3 class="header header2">Право подписи договора</h3>
				<div class="proxy">
					<form action="#" class="hidden">
						<div class="form-group">
							<label>Доверенность №</label>
							<input type="text" class="form-control" id="number" name="number" aria-label="" value="">
						</div>
						<label>Период</label>
						<div class="input-group">
							<input type="text" class="form-control input-sm datepicker" name="from">
							<span class="input-group-addon" id="sizing-addon2">-</span>
							<input type="text" class="form-control input-sm datepicker" name="to">
						</div>
					</form>
					<hr>
					<button class="btn blue btn-sm add-proxy">Добавить</button>
				</div>
			</div>
			<div class="col-md-6 proxy">
				<h3 class="header header2">Право подписи акта сверок</h3>
				<div class="proxy">
					<form action="#" class="hidden">
						<div class="form-group">
							<label>Доверенность №</label>
							<input type="text" class="form-control" id="number" name="number" aria-label="" value="">
						</div>
						<label>Период</label>
						<div class="input-group">
							<input type="text" class="form-control input-sm datepicker" name="from">
							<span class="input-group-addon" id="sizing-addon2">-</span>
							<input type="text" class="form-control input-sm datepicker" name="to">
						</div>
					</form>
					<hr>
					<button class="btn blue btn-sm add-proxy">Добавить</button>
				</div>
			</div>
		</div>
		<br><br><br>

		<div class="new-contact-form hidden">
			<div class="row">
				<form class="is-form" method="get" action="#">
					<div class="form-group">
						<label>Должность</label>
						<input type="text" class="form-control" id="pozition" name="pozition" placeholder="" value="">
					</div>
					<div class="form-group">
						<label>ФИО</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="" value="">
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>Телефон</label>
								<input type="text" class="form-control" id="phone" name="phone" aria-label="" value="">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label>Факс</label>
								<input type="text" class="form-control" id="faks" aria-label="" value="">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Мобильный телефон</label>
						<input type="tel" class="form-control" id="name" name="mobile" placeholder="" value="">
					</div>
					<div class="form-group">
						<label>Email</label>
						<input type="email" class="form-control" id="email" name="email" placeholder="" value="">
					</div>
				</form>
			</div>
			<div class="row">
				<br>
				<h2 class="header">Доверенности:</h2>
				<div class="col-md-6">
					<h3 class="header header2">Право подписи договора</h3>
					<div class="proxy">
						<form action="#" class="hidden">
							<div class="form-group">
								<label>Доверенность №</label>
								<input type="text" class="form-control" id="number" name="number" aria-label="" value="">
							</div>
							<label>Период</label>
							<div class="input-group">
								<input type="text" class="form-control input-sm datepicker" name="from">
								<span class="input-group-addon" id="sizing-addon2">-</span>
								<input type="text" class="form-control input-sm datepicker" name="to">
							</div>
						</form>
						<hr>
						<button class="btn blue btn-sm add-proxy">Добавить</button>
					</div>
				</div>
				<div class="col-md-6 proxy">
					<h3 class="header header2">Право подписи акта сверок</h3>
					<div class="proxy">
						<form action="#" class="hidden">
							<div class="form-group">
								<label>Доверенность №</label>
								<input type="text" class="form-control" id="number" name="number" aria-label="" value="">
							</div>
							<label>Период</label>
							<div class="input-group">
								<input type="text" class="form-control input-sm datepicker" name="from">
								<span class="input-group-addon" id="sizing-addon2">-</span>
								<input type="text" class="form-control input-sm datepicker" name="to">
							</div>
						</form>
						<hr>
						<button class="btn blue btn-sm add-proxy">Добавить</button>
					</div>
				</div>
			</div>
			<br><br><br>
		</div>

		<div class="btn-group btn-group-justified" role="group">
			<a href="javascript:void(0)" class="btn btn-info add-contact"><i class="glyphicon glyphicon-plus"></i> Добавить</a>
			<a class="btn btn-success demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
		</div>
		<br><br><br>
	</div>

	<script>
		$('.add-contact').on('click', function () {
			$('.new-contact-form').removeClass('hidden');
		});

		$('.add-proxy').on('click', function () {
			$(this).parent().find('form').removeClass('hidden');
		});
	</script>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>