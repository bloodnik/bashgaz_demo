<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header">Организация</h1>

		<form class="is-form" method="get" action="#">
			<div class="form-group">
				<label>Краткое наименование</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="ООО Ромашка">
			</div>
			<div class="form-group">
				<label>Полное наименование</label>
				<input type="text" class="form-control" id="full-name" name="full-name" placeholder="Общество с ограниченной ответственностью Ромашка">
			</div>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>ИНН</label>
						<input type="text" class="form-control" id="inn" name="inn" aria-label="" value="02648949849">
					</div>
					<div class="form-group">
						<label>ОКПО</label>
						<input type="text" class="form-control" id="okpo" name="okpo" value="16516">
					</div>
					<div class="form-group">
						<label>ОГРН</label>
						<input type="text" class="form-control" id="ogrn" name="ogrn" value="16516">
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label>КПП</label>
						<input type="text" class="form-control" id="kpp" aria-label="" value="0219485494">
					</div>
					<div class="form-group">
						<label>ОКВЭД</label>
						<input type="text" class="form-control" id="okved" name="okved" aria-label="" value="5605">
					</div>
					<div class="form-group">
						<label>ОКАТО</label>
						<input type="text" class="form-control" id="okato" name="okato" aria-label="" value="5605">
					</div>
				</div>
			</div>
			<br>
			<div class="btn-group btn-group-justified" role="group">
				<a class="btn btn-success demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
				<a class="btn btn-default light-blue" href="bank.php">Далее <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a>
			</div>
		</form>


	</div>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>