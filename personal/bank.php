<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/header.php" ?>

<? require_once "left_menu.php" ?>

	<div class="col-md-9" style="margin-top: 15px">
		<h1 class="header">Банковские реквизиты</h1>

		<form class="is-form" method="get" action="#">
			<div class="form-group">
				<label>Наименование</label>
				<input type="text" class="form-control" id="name" name="name" placeholder="Альфа-Банк" value="Альфа-Банк">
			</div>
			<div class="form-group">
				<label>Расчетный счет</label>
				<input type="text" class="form-control" id="acc-num" name="acc-num" placeholder="" value="16894894984988998946">
			</div>
			<div class="form-group">
				<label>Корр. счет</label>
				<input type="text" class="form-control" id="kor-num" name="kor-num" placeholder="" value="88749846549465494">
			</div>
			<div class="form-group">
				<label>БИК</label>
				<input type="text" class="form-control" id="bik" name="bik" placeholder="" value="998794944">
			</div>

			<br>
			<div class="btn-group btn-group-justified" role="group">
				<a class="btn btn-success demo">Сохранить <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span></a>
				<a class="btn btn-default light-blue" href="address.php">Далее <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a>
			</div>
		</form>


	</div>

<? require_once $_SERVER['DOCUMENT_ROOT'] . "/view/footer.php" ?>